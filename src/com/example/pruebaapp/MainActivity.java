/**
 * Creamos la clase MainActivity para definir los métodos que nos daran la información requerida
 * @author Luis Raymundo Solís Peralta / pepperray01@gmail.com
 * @version 1.0
 * @date Abril 2016
 */

package com.example.pruebaapp;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TableRow.LayoutParams;


public class MainActivity extends Activity {
	
	//Atributos de la clase.
	
	private static final String SOAP_ACTION = "http://opi.fuller.com.mx/Test/GetUsuariosActivos";//Nombre de la acción.
	private static final String METHOD_NAME = "GetUsuariosActivos";//Nombre del método a ejecutar.
	private static final String NAMESPACE = "http://opi.fuller.com.mx/Test/";//Nombre del espacio.
	private static final String URL = "http://104.239.175.163:8083/test/test.asmx";//La dirección que aloja el servicio.
	private Cliente[] listaClientes; //Arreglo que almacena los clientes
	//private ListView lstClientes;
	//private GridView numFuller;
	//GridView grdOpciones;
	private TableLayout tl; //La tabla donde se mostraran los clientes
	private TableRow tr;// Las columnas de la tabla.
	//private TextView valueTV, Nom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //Permisos para ejecutar la app desde la clase principal
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
		.permitAll().build();
        StrictMode.setThreadPolicy(policy);
        
        Button Llamada = (Button) findViewById(R.id.btnLlamado);//Se asigna el boton.
        
        tl = (TableLayout) findViewById(R.id.maintable);//Asignamos la tabla

        //Accion al presionar el boton
        Llamada.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Asignamos el parámetro que pasaremos en el WS
				
				String peticion = "7281";
				
				//Creamos la propiedad a mandar
				PropertyInfo zona = new PropertyInfo();
				
				zona.name = "zona";
				
				zona.type = Integer.class;
				
				
				zona.setValue(peticion);
				
				//Creamos la solicitud SOAP
				
				SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
				
				request.addProperty(zona);
				
				String reqString = request.toString();
				
				Log.i("LRSP Mando esto---", reqString.toString());
				
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				
				
				envelope.setOutputSoapObject(request);
				
				envelope.dotNet = true;
				
				HttpTransportSE httpTransport = new HttpTransportSE(URL);
				
				try {
					
					httpTransport.call(SOAP_ACTION, envelope);
					
					Toast.makeText(getApplicationContext(), "Enviado", Toast.LENGTH_SHORT).show();
					
					//Obtenemos la respuesta del WS
					
					SoapObject result = (SoapObject) envelope.getResponse();
					
					listaClientes = new Cliente[result.getPropertyCount()];
					
					for (int i = 0; i < listaClientes.length; i++) 
					{
				           SoapObject ic = (SoapObject)result.getProperty(i);
				            
				           Cliente cli = new Cliente();
				           cli.NumeroFullerette = Integer.parseInt(ic.getProperty(0).toString());
				           cli.DigitoVerificador = Integer.parseInt(ic.getProperty(1).toString());
				           cli.Nombre = ic.getProperty(2).toString();
				            
				           listaClientes[i] = cli;
				           
					}
						       				
					Log.i("RAYO La respuesta es ----", result.toString());
					
				}
				catch(Exception e){
					
					e.printStackTrace();
					
					Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
				}
				
				/** Creamos una fila dinamica que seran los encabezados**/
		        tr = new TableRow(MainActivity.this);
		        
		        tr.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
		        
		        /** Creamos un TextView para agregar la columna **/
		        TextView number1 = new TextView(MainActivity.this);
		        
		        number1.setText("#Fullrette");
		        
		        number1.setTextColor(Color.GRAY);
		        
		        number1.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
		        
		        number1.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
		        
		        number1.setPadding(5, 5, 5, 0);
		        
		        tr.addView(number1);  // Agregamos el textView a la fila.
		 
		        /** Creamos otro TextView para mostrar la sigiente columna **/
		        
		        TextView dvNumber = new TextView(MainActivity.this);
		        
		        dvNumber.setText("DV");
		        
		        dvNumber.setTextColor(Color.GRAY);
		        
		        dvNumber.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
		        
		        dvNumber.setPadding(5, 5, 5, 0);
		        
		        dvNumber.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
		        
		        tr.addView(dvNumber); //  Agregamos el textView a la fila.
		        
		        /** Creamos otro TextView para mostrar la sigiente columna **/
		        
		        TextView Nom = new TextView(MainActivity.this);
		        
		        Nom.setText("Nombre");
		        
		        Nom.setTextColor(Color.GRAY);
		        
		        Nom.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
		        
		        Nom.setPadding(5, 5, 5, 0);
		        
		        Nom.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
		        
		        tr.addView(Nom); //  Agregamos el textView a la fila.
				
		        tl.addView(tr, new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
		 
		        //Creamos la siguiente fila
		        tr = new TableRow(MainActivity.this);
		        
		        tr.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
				
				final Integer[] numFull = new Integer[listaClientes.length];
				
				final Integer[] dv = new Integer[listaClientes.length];
				
				final String[] nombreCl = new String[listaClientes.length];
				
				for(int i=0; i<listaClientes.length; i++){
					numFull[i] = listaClientes[i].NumeroFullerette;//Llenamos el arreglo que contendra el numero de fullerette
					
					dv[i] = listaClientes[i].DigitoVerificador;//Llenamos el arreglo que contendra el numero verificador
					 
					nombreCl[i] = listaClientes[i].Nombre;//Llenamos el arreglo con los nombre de clientes
					
					
					/** Creamos una fila dinamica que seran los datos **/
		            tr = new TableRow(MainActivity.this);
		            tr.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
		            
		            /** Creamos otro TextView para mostrar la sigiente columna **/
		            number1 = new TextView(MainActivity.this);
		            number1.setText(numFull[i].toString());
		            number1.setTextColor(Color.BLACK);
		            number1.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
		            number1.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
		            number1.setPadding(5, 5, 5, 5);
		            tr.addView(number1);  // Agregamos el textView a la fila.
		            
		            /** Creamos otro TextView para mostrar la sigiente columna **/
		            dvNumber = new TextView(MainActivity.this);
		            dvNumber.setText(dv[i].toString());
		            dvNumber.setTextColor(Color.BLACK);
		            dvNumber.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
		            dvNumber.setPadding(5, 5, 5, 5);
		            dvNumber.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
		            tr.addView(dvNumber); // Adding textView to tablerow.
		            
		            /** Creamos otro TextView para mostrar la sigiente columna **/
		            Nom = new TextView(MainActivity.this);
		            Nom.setText(nombreCl[i]);
		            Nom.setTextColor(Color.BLACK);
		            Nom.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
		            Nom.setPadding(5, 5, 5, 5);
		            Nom.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
		            tr.addView(Nom); // Agregamos el textView a la fila.
		            
		            // Agregamos la fila a la tabla
		            tl.addView(tr, new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
				}

			}

		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
