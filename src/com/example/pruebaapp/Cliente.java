/**
 * Creamos la clase Cliente para darle estructura a nuestros clientes que recibiremos
 * @author Luis Raymundo Solís Peralta / pepperray01@gmail.com
 * @version 1.0
 * @date Abril 2016
 */
package com.example.pruebaapp;

import org.ksoap2.serialization.PropertyInfo;

public class Cliente {
	
	//Atributos de la clase Cliente
	
	public String Nombre;
	int NumeroFullerette;
	int DigitoVerificador;
	
	
	//Metodo inicializador por omision
	
	public Cliente()
	{
		NumeroFullerette = 0;
		Nombre = "";
		DigitoVerificador = 0;
	}
	
	/*
	 * Metodo inicializador de Cliente 
	 * @param int NumeroFullerette - El numero de fullerette
	 * @param int DigitoVerificador - El digito verificador
	 * @param String Nombre - El nombre del cliente
	 */
	
	public Cliente(int NumeroFullerette, int DigitoVerificador, String Nombre)
	{
		this.NumeroFullerette = NumeroFullerette;
		this.DigitoVerificador = DigitoVerificador;
		this.Nombre = Nombre;
	}
	
	/*
	 * Metodo para obtener la propiedad del objeto
	 * @param int arg0 - El argumento
	 * @return Object el objeto que esta dentro del arg0
	 */
	public Object getProperty(int arg0) {

		switch(arg0)
        {
        case 0:
            return NumeroFullerette;
        case 1:
            return DigitoVerificador;
        case 2:
            return Nombre;
        }
		
		return null;
	}
	
	public int getPropertyCount() {
		return 3;
	}
	
	/*
	 * Metodo para obtener la propiedad del objeto
	 * @param int ind - El argumento indice 
	 * @param PropertyInfo info - La informacion de esa propiedad
	 */
	
	public void getPropertyInfo(int ind,  PropertyInfo info) {
		switch(ind)
        {
	        case 0:
	            info.type = PropertyInfo.INTEGER_CLASS;
	            info.name = "NumeroFullerette";
	            break;
	        case 1:
	        	info.type = PropertyInfo.INTEGER_CLASS;
	            info.name = "DigitoVerificador";
	            break;
	        case 2:
	            info.type = PropertyInfo.STRING_CLASS;
	            info.name = "Nombre";
	            break;
	        default:break;
        }
	}
	
}
